///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package at.htlpinkafeld.horizon.kalenderverwaltungstool.service;
//
//import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Room;
//import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Appointment;
//import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Employee;
//import java.util.List;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Stefan Krojer
// */
//public class ServiceTest {
//
//    public ServiceTest() {
//    }
//
//    /**
//     * Test of addUser method, of class Service.
//     */
//    @Test
//    public void testAddUser() {
//        System.out.println("addUser");
//        Employee u = new Employee("Stefan", "Krojer", "123", "stefodo.825@gmail.com", "Sales", false);
//        Service instance = Service.getInstance();
//        instance.addUser(u);
//        int x = 4;
//        assertEquals(x, instance.getUserList().size());
//    }
//
//    /**
//     * Test of addRoom method, of class Service.
//     */
//    @Test
//    public void testAddRoom() {
//        System.out.println("addUser");
//        Room r = new Room("Testraum", 1,1);
//        Service instance = Service.getInstance();
//        instance.addRoom(r);
//        int x = 1;
//        assertEquals(x, instance.getRoomList().size());
//    }
//
//    /**
//     * Test of removeUser method, of class Service.
//     */
//    @Test
//    public void testRemoveUser() {
//        System.out.println("removeUser");
//        int id = 0;
//        Service instance = Service.getInstance();
//        instance.removeUser(id);
//        assertEquals(3, instance.getUserList().size());
//    }
//
//    @Test
//    public void testRemoveRoom() {
//        System.out.println("removeRoom");
//        Room r = new Room("Testraum", 1,1);
//        Service instance = Service.getInstance();
//        instance.addRoom(r);
//        instance.removeRoom(0);
//        assertEquals(1, instance.getRoomList().size());
//
//    }
////    /**
////     * Test of updateUser method, of class Service.
////     */
//
//    @Test
//    public void testUpdateUser() {
//        System.out.println("updateUser");
//        Employee update = new Employee("Stefan", "Krojer", "123", "stefodo.825@gmail.com", "Sales", false);
//        Service instance = Service.getInstance();
//        String newname = "Koch";
//        instance.updateUser(update, new Employee("Stefan", newname, "123", "stefodo.825@gmail.com", "Sales", false));
//        assertEquals(instance.getUser(update.getUserID()).getLname(), newname);
//    }
////
////    /**
////     * Test of updateRoom method, of class Service.
////     */
//
//    @Test
//    public void testUpdateRoom() {
//        System.out.println("updateRoom");
//        Room update = new Room("Testraum", 1,1);
//        Service instance = Service.getInstance();
//        instance.updateRoom(update, new Room("Testraum", 3,3));
//
//        assertEquals(instance.getRoom(update.getRoomID()).getCountStanding(),3, 3);
//
//    }
////
////    /**
////     * Test of getUser method, of class Service.
////     */
//
//    @Test
//    public void testGetUser_String() {
//        System.out.println("getUser");
//        String Name = "Herbert Krojer";
//        Service instance = Service.getInstance();
//
//        Employee expResult = new Employee("Herbert", "Krojer", "123", "stefodo.825@gmail.com", "Sales", false);
//
//        instance.addUser(expResult);
//        Employee result = instance.getUser(Name);
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of getUser method, of class Service.
//     */
//    @Test
//    public void testGetUser_int() {
//        System.out.println("getUser");
//
//        Service instance = Service.getInstance();
//
//        Employee expResult = new Employee("Günther", "Krojer", "123", "stefodo.825@gmail.com", "Sales", false);
//
//        instance.addUser(expResult);
//        Employee result = instance.getUser(expResult.getUserID());
//        assertEquals(expResult.getUserID(), result.getUserID());
//
//    }
//    
//
////    /**
////     * Test of sendMail method, of class Service.
////     */
//   /* @Test
//    public void testSendMail() {
//        System.out.println("sendMail");
//        String title = "Unit Tests werden ausgeführt";
//        String message = "Hallo! Diese Mail wurde mittels Unit Test versendet. \n Einen schönen Tag noch!";
//        Service instance = Service.getInstance();
//        assertTrue(instance.sendMail(instance.getUser("Lukas Hatzl").getId(), title, message));
//    }*/
//}
//
////
////    /**
////     * Test of readCSV method, of class Service.
////     */
//

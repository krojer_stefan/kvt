/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
//import net.fortuna.ical4j.model.property.Duration;

@Entity
@Table(name = "appointment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Appointment.findAll", query = "SELECT a FROM Appointment a")
    , @NamedQuery(name = "Appointment.findByAppid", query = "SELECT a FROM Appointment a WHERE a.appid = :appid")
    , @NamedQuery(name = "Appointment.findByAllday", query = "SELECT a FROM Appointment a WHERE a.allday = :allday")
    , @NamedQuery(name = "Appointment.findByCreatedinkvt", query = "SELECT a FROM Appointment a WHERE a.createdinkvt = :createdinkvt")
    , @NamedQuery(name = "Appointment.findByDate", query = "SELECT a FROM Appointment a WHERE a.date = :date")
    , @NamedQuery(name = "Appointment.findByDuration", query = "SELECT a FROM Appointment a WHERE a.duration = :duration")
    , @NamedQuery(name = "Appointment.findByStartsat", query = "SELECT a FROM Appointment a WHERE a.startsat = :startsat")})

public class Appointment implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "appointment")
    private Collection<Approom> approomCollection;
   
     private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "appid")
    private Integer appid;
    @Column(name = "allday")
    private Boolean allday;
    @Column(name = "createdinkvt")
    private Boolean createdinkvt;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "duration")
    private Double duration;
    @Column(name = "startsat")
    private Double startsat;
    @JoinColumn(name = "employeeid", referencedColumnName = "employeeid")
    @ManyToOne
    private Employee employeeid;
    @JoinColumn(name = "roomid", referencedColumnName = "roomid")
    @ManyToOne
    private Room roomid;



    public Appointment(){}
    public Appointment(Date date, Double startsAt, double duration, Employee belongsTo, boolean KVT) {
        this.date = date;
        this.startsat = startsAt;
        this.duration = duration;
        this.employeeid = belongsTo;
        this.createdinkvt = KVT;
    }
    public Appointment(int id, boolean allday, Employee belongsto, Room roomid, Date date, Double startsAt, double duration, boolean KVT) {
        this.appid = id;
        this.allday = allday;
        this.employeeid = belongsto;
        this.roomid = roomid;
        this.date = date;
        this.duration = duration;
        this.startsat = startsAt;
        this.createdinkvt = KVT;
        
    }
    public Appointment(Date date, Double startsAt, double duration, Employee belongsTo, boolean KVT, boolean allday) {
        this.date = date;
        this.startsat = startsAt;
        this.duration = duration;
        this.employeeid = belongsTo;
        this.createdinkvt = KVT;
        this.allday = allday;
    }

    public Integer getAppID() {
        return appid;
    }

    public void setAppID(Integer appID) {
        this.appid = appID;
    }

    public Boolean getAllday() {
        return allday;
    }

    public void setAllday(Boolean allday) {
        this.allday = allday;
    }

    public Boolean getCreatedInKVT() {
        return createdinkvt;
    }

    public void setCreatedInKVT(Boolean createdInKVT) {
        this.createdinkvt = createdInKVT;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public Double getStarsAt() {
        return startsat;
    }

    public void setStarsAt(Double starsAt) {
        this.startsat = starsAt;
    }

    public Room getRoom() {
        return roomid;
    }

    public void setRoom(Room roomID) {
        this.roomid = roomID;
    }

    public Employee getBelongsTo() {
        return employeeid;
    }

    public void setBelongsTo(Employee belongsTo) {
        this.employeeid = belongsTo;
    }

    @Override
    public String toString() {
        return "Appointment{" + "appID=" + appid + ", allday=" + allday + ", createdInKVT=" + createdinkvt + ", date=" + date + ", duration=" + duration + ", starsAt=" + startsat + ", roomID=" + roomid + ", belongsTo=" + employeeid + '}';
    }

    @XmlTransient
    public Collection<Approom> getApproomCollection() {
        return approomCollection;
    }

    public void setApproomCollection(Collection<Approom> approomCollection) {
        this.approomCollection = approomCollection;
    }



  
    

    

    
    
    
    
    
    
    
}

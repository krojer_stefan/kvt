/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.db;

import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Room;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transactional;

public class RoomDao {

    private static final String PERSISTENCE_UNIT_NAME = "kvt";
    private static EntityManagerFactory factory;
    private EntityManager em;

    public RoomDao() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        em = factory.createEntityManager();
    }

    public List<Room> getAllRooms() {
        Query q = em.createNamedQuery("Room.findAll");
        System.out.println(q.getResultList().size());
        List<Room> rl = (List<Room>) q.getResultList();

        return rl;
    }
    
    public void addRoom(Room r) {
        try {
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(r);
            entr.commit();
        } finally {
            em.close();
        }
    }

    public void deleteRoom(Room r) {
        EntityTransaction entr = em.getTransaction();
        entr.begin();
        Query query = em.createQuery("DELETE FROM Room c WHERE c.roomID = :r");
        query.setParameter("r", r.getRoomID()).executeUpdate();
        entr.commit();
    }
    
    public void updateRoom(Room prev, Room r){
        Room room = em.find(Room.class, prev.getRoomID());
        em.getTransaction().begin();
        room.setRoomname(r.getRoomname());
        room.setCountSeats(r.getCountSeats());
        room.setCountStanding(r.getCountStanding());
        em.getTransaction().commit();
    }
    
    public Room getRoomByID(int roomID) {
        Room r = (Room) em.find(Room.class, roomID);

        return r;
    }

    public Room getRoomByName(String name) {
        Query q = em.createNamedQuery("Room.findAll");
        List<Room> rl = (List<Room>) q.getResultList();

        for (int i = 0; i < rl.size(); i++) {
            if (rl.get(i).getRoomname().equals(name)) {
                return rl.get(i);
            }
        }
        return null;
    }

    public List<Room> getRoomsBySeats(int min) {
        Query q = em.createNamedQuery("Room.findAll");
        List<Room> rl = (List<Room>) q.getResultList();
        List<Room> re = new ArrayList<>();
        for (Room r : rl) {
            if (r.getCountSeats() >= min) {
                re.add(r);
            }
        }
        return re;
    }

    public List<Room> getRoomsByStandings(int min) {
        Query q = em.createNamedQuery("Room.findAll");
        List<Room> rl = (List<Room>) q.getResultList();
        List<Room> re = new ArrayList<>();
        for (Room r : rl) {
            if (r.getCountStanding() >= min) {
                re.add(r);
            }
        }
        return re;
    }

    public List<Room> getRoomsByCapacity(int min) {
        Query q = em.createNamedQuery("Room.findAll");
        List<Room> rl = (List<Room>) q.getResultList();
        List<Room> re = new ArrayList<>();
        for (Room r : rl) {
            if ((r.getCountSeats() + r.getCountStanding()) >= min) {
                re.add(r);
            }
        }
        return re;
    }

    

}

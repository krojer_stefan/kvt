/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.service;

import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.MailClient;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Room;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Appointment;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Approom;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Employee;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.db.AppRoomDao;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.db.AppointmentDao;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.db.RoomDao;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.db.EmployeeDao;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.ChronoUnit.MINUTES;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Stefan Krojer
 */


@ManagedBean(name = "service", eager = true)
@SessionScoped
public class Service {

    public int getuIDCounter() {
        return uIDCounter;
    }

    public void setuIDCounter(int uIDCounter) {
        this.uIDCounter = uIDCounter;
    }

    public int getRoomIDCounter() {
        return roomIDCounter;
    }

    public void setRoomIDCounter(int roomIDCounter) {
        this.roomIDCounter = roomIDCounter;
    }

    public MailClient getmClient() {
        return mClient;
    }

    public void setmClient(MailClient mClient) {
        this.mClient = mClient;
    }

    private Map<Integer, Employee> userList = new HashMap();
    private int uIDCounter = 0;
    private Map<Integer, Room> roomList = new HashMap();
    private int roomIDCounter = 0;
    private MailClient mClient = new MailClient();
    private List<Appointment> allTermine = new ArrayList();

    private Employee logUs;

    public boolean addUser(Employee u) {
        u.setUserID(uIDCounter);
        userList.put(uIDCounter, u);
        uIDCounter++;

        return userList.get(uIDCounter - 1) == u;

    }

    public boolean addRoom(Room r) {
        r.setRoomID(roomIDCounter);
        roomList.put(roomIDCounter, r);
        roomIDCounter++;

        return roomList.get(roomIDCounter - 1) == r;

    }

    public void removeUser(int id) {
        userList.remove(id);
    }

    public void removeRoom(int id) {
        roomList.remove(id);
    }

    public void updateUser(Employee update, Employee newData) {
        userList.put(update.getUserID(), newData);

    }

    public void updateRoom(Room update, Room newData) {
        roomList.put(update.getRoomID(), newData);

    }

    public Room getRoom(int id) {
        return this.roomList.get(id);
    }

    public Map<Integer, Employee> getUserList() {
        return userList;
    }

    public Map<Integer, Room> getRoomList() {
        return roomList;
    }

    public Employee getUser(String Name) {
        String fn = Name.split(" ")[0];
        String ln = Name.split(" ")[1];

        for (Employee u : userList.values()) {

            if (u.getFname().equals(fn) && u.getLname().equals(ln)) {
                System.out.println(u.getFname());
                return u;
            }

        }
        return null;
    }

    public Employee getUser(int id) {
        return userList.get(id);
    }

    public boolean sendMail(int recipientID, String title, String message) {
        String mail = userList.get(recipientID).getMail();
        return mClient.sendMail(mail, message, title);
    }

    public List<String> readCSV(String path) throws IOException {
        BufferedReader buffRead;
        try {
            buffRead = new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException ex) {
            System.out.println("File not found, returning null");
            return null;
        }
        List<String> termine = new ArrayList();
        buffRead.readLine();
        while (buffRead.ready()) {
            termine.add(buffRead.readLine());
        }
        return termine;
    }

    public Employee getLogUs() {
        return logUs;
    }

    public void setLogUs(Employee logUs) {
        this.logUs = logUs;
    }

    public void mapTermine(Employee currLoggedIn, List<String> readCSV) {
        String[] parsedData;
        double duration;
        double minutes;
        LocalDate localDate;
        Date date;
        String d1, d2, locdat, name;
        System.out.println("Termine: " + readCSV.size());
        for (String csv : readCSV) {
            try {
                parsedData = csv.split(",");
                locdat = parsedData[1].split("\"")[2];
                d1 = parsedData[2].split("\"")[2];
                d2 = parsedData[4].split("\"")[2];

                System.out.println("Entry: " + parsedData[1] + " " + parsedData[2] + " " + parsedData[4]);
                System.out.println("Actual: " + locdat + " " + d1 + " " + d2);

                localDate = LocalDate.parse(locdat, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                date = asDate(localDate);

                minutes = MINUTES.between(LocalTime.parse(d1, DateTimeFormatter.ofPattern("HH:mm:ss")),
                        LocalTime.parse(d2, DateTimeFormatter.ofPattern("HH:mm:ss")));
                duration = minutes / 60;

                System.out.println("Parsed Data: " + date.toString() + " " + minutes + " " + duration);

                allTermine.add(new Appointment(date, Double.parseDouble(d1), duration, this.getUser(currLoggedIn.getFname() + " " + currLoggedIn.getLname()), false));
                //allTermine.add(new Appointment(date, LocalTime.parse(d1, DateTimeFormatter.ofPattern("HH:mm:ss")), duration, currLoggedIn.getFname() + " " + currLoggedIn.getLname(), false));
            } catch (Exception e) {
                System.out.println("Fehler! " + e.getMessage());
            }

        }
    }

    public List<Appointment> getAllTermine() {
        return allTermine;
    }

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }
    
    public void dbConnection(){
        /*
        AppointmentDao ad = new AppointmentDao();
        RoomDao rd = new RoomDao();
        EmployeeDao ed = new EmployeeDao();
        Date d = new Date(120, 3, 20);
        
        Employee e= ed.getEmployeeById(1);
        Room r = rd.getRoomByID(4);
        Appointment a = new Appointment(5, true, e, r, d, 9.0, 3.5, false);
        Room r3 = rd.getRoomByID(3);
        Appointment b = new Appointment(5, false, e, r3, d, 9.0, 2.5, true);
        //ad.addAppointment(a);
        
        ad.deleteAppointment(a);        
        
        
        /*
        RoomDao rd = new RoomDao();
        EmployeeDao ed = new EmployeeDao();
        AppointmentDao ad = new AppointmentDao();
        
        List<Room> rl = rd.getAllRooms();
        printRooms(rl);
        /*
        Appointment a = ad.getByAppointmentId(1);
        Employee e = ed.getEmployeeById(1);
        ad.updateAppointment(a, new Appointment(new Date(120, 6, 17), null, 0, e, false, true));
        
        /*
        Date d = new Date(120, 3, 20);
        Employee e= ed.getEmployeeById(1);
        Room r = rd.getRoomByID(4);
        Appointment b = new Appointment(4, true, e, r, d, 9.0, 3.5, false);
        
       // ad.addAppointment(b); ad.deleteAppointment(b);
        
        List<Appointment> al = new ArrayList<>();
        /*al = ad.getAllAppointments();
        //printAppointments(al);
        /*
        Appointment a = ad.getByAppointmentId(2);
        //printAppointment(a);
        
        al = ad.getByAllDay();
        //printAppointments(al);
        
        al = ad.getByKVT();
        //printAppointments(al); 
        System.out.println("DATE");
        al = ad.getByDate(b.getDate());*/
        /*System.out.println("Duration");
        System.out.println("Duration: " + b.getDuration());
        al = ad.getByDuration(b.getDuration());
        printAppointments(al);*/
        /*
        al = ad.getByStartsAt(b);
        System.out.println("" + b.getStarsAt());
        System.out.println("SIZE: " + al.size());
        printAppointments(al);
        
        al = ad.getStartsAtTime(12);
        printAppointments(al);*/
        
        /*
        //---------------------------------------------------------------------------------------------------------------
        //EmployeeDao Test
        *//*
        Employee e = ed.getEmployeeById(1);
        ed.updateEmployee(e, new Employee(1, "Julian  Robert", "Koch", "asdf123!", "koch.julianr@gmail.com", "IT", true));
        /*
        List<Employee> empList = new ArrayList<>();
        Employee e;
        Employee a = new Employee(4, "Benjamin", "Antos", "123", "benjiwrestling@gmail.com", "HR", false);
        
        //ed.addEmployee(a);
        ed.deleteEmployee(a);
        
        /*
        System.out.println("Get All Employees - Expected 3");
        empList = ed.getAllEmployees();
        printEmployees(empList);
        
        System.out.println("Employee - ID = 2");
        e = ed.getEmployeeById(2);
        printEmployee(e);
        
        System.out.println("Get Employees By Name - Expected 1");
        empList = ed.getEmployeeByName("Julian", "Koch");
        printEmployees(empList);
        
        System.out.println("Print all Admins: - Expected 1");
        empList = ed.getAllAdmins();
        printEmployees(empList);
        
        System.out.println("Department IT - Expected 3");
        empList = ed.getEmployeesByDepartment("IT");
        printEmployees(empList);
        
        //---------------------------------------------------------------------------------------------------------------
        //RoomDao Test
        /* 
        Room r1 = new Room("Toronto", 10, 0);
        r1.setRoomID(7);
        rd.deleteRoomByID(r1);
         */
        //rd.addRoom(r1);
        /*
        RoomDao d = new RoomDao();
        List<Room> r = new ArrayList<>();
        r = d.getAllRooms();
        System.out.println("-------------------------");
        System.out.println("Get all rooms: ");
        print(r);
        System.out.println("-------------------------");
        /*
        Room room = rd.getRoomByID(3);
        System.out.println("-------------------------");
        System.out.println("Get Room By ID: ");
        printRoom(room);
        System.out.println("-------------------------");
        /*
        room = d.getRoomByName("Hartberg");
        System.out.println("-------------------------");
        System.out.println("Get Room By Name: ");
        printRoom(room);
        System.out.println("-------------------------");
        
        r = d.getRoomsBySeats(20);
        System.out.println("-------------------------");
        System.out.println("Get Rooms By Seats: - Condition: 20 seats"  );
        print(r);
        System.out.println("-------------------------");
        
        r = d.getRoomsByStandings(30);
        System.out.println("-------------------------");
        System.out.println("Get Rooms By Standings: - Condition: 30 standings");
        print(r);
        System.out.println("-------------------------");
        
         r = d.getRoomsByCapacity(60);
        System.out.println("-------------------------");
        System.out.println("Get Rooms By Standings: - Condition: space for 60");
        print(r);
        System.out.println("-------------------------");
         */
        /*
        Room hb = rd.getRoomByName("Hartberg");
        rd.updateRoom(hb, new Room("Oberwart", 70, 70));*/
    }

    public void printRooms(List<Room> roomList) {
        for (Room r : roomList) {
            System.out.println("----------------");
            System.out.println("RoomID:     " + r.getRoomID());
            System.out.println("Roomname:   " + r.getRoomname());
            System.out.println("Seats:      " + r.getCountSeats());
            System.out.println("Standings:  " + r.getCountStanding());
            System.out.println("----------------");
        }
    }

    public void printRoom(Room r) {
        System.out.println("----------------");
        System.out.println("RoomID:     " + r.getRoomID());
        System.out.println("Roomname:   " + r.getRoomname());
        System.out.println("Seats:      " + r.getCountSeats());
        System.out.println("Standings:  " + r.getCountStanding());
        System.out.println("----------------");

    }

    public void printEmployee(Employee e) {
        System.out.println("----------------");
        System.out.println("UserID:        " + e.getUserID());
        System.out.println("Firstname:     " + e.getFname());
        System.out.println("Lastname:      " + e.getLname());
        System.out.println("Mail:          " + e.getMail());
        System.out.println("Password:      " + e.getPassword());
        System.out.println("Admin:         " + e.getIsAdmin());
        System.out.println("Department:    " + e.getDept());
        System.out.println("----------------");
    }

    public void printEmployees(List<Employee> employeeList) {
        for (Employee e : employeeList) {
        System.out.println("----------------");
        System.out.println("UserID:        " + e.getUserID());
        System.out.println("Firstname:     " + e.getFname());
        System.out.println("Lastname:      " + e.getLname());
        System.out.println("Mail:          " + e.getMail());
        System.out.println("Password:      " + e.getPassword());
        System.out.println("Admin:         " + e.getIsAdmin());
        System.out.println("Department:    " + e.getDept());
        System.out.println("----------------");
        }
    }
    
    public void printAppointment(Appointment a) {
        System.out.println("----------------");
        System.out.println("AppID:          " + a.getAppID());
        System.out.println("Allday:         " + a.getAllday());
        System.out.println("CreatedInKVT:   " + a.getCreatedInKVT());
        System.out.println("Date:           " + a.getDate());
        System.out.println("Duration:       " + a.getDuration());
        System.out.println("Starts at:      " + a.getStarsAt());
        System.out.println("Belongs to:     " + a.getBelongsTo());
        System.out.println("Room-ID:        " + a.getRoom());
        System.out.println("----------------");
    }

    public void printAppointments(List<Appointment> app) {
        for (Appointment a : app) {
            System.out.println("----------------");
            System.out.println("AppID:          " + a.getAppID());
            System.out.println("Allday:         " + a.getAllday());
            System.out.println("CreatedInKVT:   " + a.getCreatedInKVT());
            System.out.println("Date:           " + a.getDate());
            System.out.println("Duration:       " + a.getDuration());
            System.out.println("Starts at:      " + a.getStarsAt());
            System.out.println("Belongs to:     " + a.getBelongsTo());
            System.out.println("Room-ID:        " + a.getRoom());
            System.out.println("----------------");
        }
    }

}

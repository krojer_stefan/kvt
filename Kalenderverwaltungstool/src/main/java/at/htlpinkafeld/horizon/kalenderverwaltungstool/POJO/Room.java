    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "room")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Room.findAll", query = "SELECT r FROM Room r")
    , @NamedQuery(name = "Room.findByRoomid", query = "SELECT r FROM Room r WHERE r.roomid = :roomid")
    , @NamedQuery(name = "Room.findByCountseats", query = "SELECT r FROM Room r WHERE r.countseats = :countseats")
    , @NamedQuery(name = "Room.findByCountstanding", query = "SELECT r FROM Room r WHERE r.countstanding = :countstanding")
    , @NamedQuery(name = "Room.findByRoomname", query = "SELECT r FROM Room r WHERE r.roomname = :roomname")})

public class Room implements Serializable{

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "room")
    private Collection<Approom> approomCollection;
  
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "roomid")
    private Integer roomid;
    @Column(name = "countseats")
    private Integer countseats;
    @Column(name = "countstanding")
    private Integer countstanding;
    @Column(name = "roomname")
    private String roomname;
    @OneToMany(mappedBy = "roomid")
    private Collection<Appointment> appointmentCollection;   
   
    public Room(String Name, int anz,int anz2) {
        this.roomname = Name;
        this.countseats = anz;
        this.countstanding = anz2;
    }
    
    public Room() {
        this.roomname = "undefined";
    }

    public Integer getRoomID() {
        return roomid;
    }

    public void setRoomID(Integer roomID) {
        this.roomid = roomID;
    }

    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname;
    }

    public Integer getCountSeats() {
        return countseats;
    }

    public void setCountSeats(Integer countSeats) {
        this.countseats = countSeats;
    }

    public Integer getCountStanding() {
        return countstanding;
    }

    public void setCountStanding(Integer countStanding) {
        this.countstanding = countStanding;
    }

    public Collection<Appointment> getAppointmentCollection() {
        return appointmentCollection;
    }

    public void setAppointmentCollection(Collection<Appointment> appointmentCollection) {
        this.appointmentCollection = appointmentCollection;
    }

  
    
    /*
    public List<Appointment> getOccupation() {
        return occupied;
    }

    public void setOccupation(List<Appointment> belegt) {
        this.occupied = belegt;
    }

    private boolean addOccupation(Appointment t){
       for(int i = 0; i<occupied.size();i++){
           if(t == occupied.get(i)){
               return false;
           }
       }
       return occupied.add(t);
    }*/

    @Override
    public String toString() {
        return "Room{" + "roomID=" + roomid + ", roomname=" + roomname + ", countSeats=" + countseats + ", countStanding=" + countstanding + ", appointmentCollection=" + appointmentCollection + '}';
    }

    @XmlTransient
    public Collection<Approom> getApproomCollection() {
        return approomCollection;
    }

    public void setApproomCollection(Collection<Approom> approomCollection) {
        this.approomCollection = approomCollection;
    }




   
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.bean;

import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Room;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Employee;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Appointment;

import at.htlpinkafeld.horizon.kalenderverwaltungstool.service.Service;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Stefan Krojer
 */
@ManagedBean
@SessionScoped
public class LoginBean {

    @ManagedProperty(value =  "#{service}")
    private Service service;

    private String firstName;
    private String lastName;
    private String pwd;
    private String logInMSG = "Bitte geben Sie Ihren Namen und Ihr Passwort ein.";

    public Service getService() {
        return service;
    }

    public void setService(Service  service) {
        this.service = service;
    }


    public LoginBean() {

    }

    public String login() {
           service.addUser(new Employee("Stefan", "Krojer", "123", "stefodo.825@gmail.com", "Sales", false));
        Employee toLogIn = service.getUser(firstName + " " + lastName);
        System.out.println(toLogIn);
        if (toLogIn == null) {
            System.out.println("null");
            logInMSG = "Nutzer nicht gefunden. Bitte nochmal versuchen";
            return null;
        } else {
                 service.dbConnection();
            if (toLogIn.getPassword().equals(pwd)) {
                service.setLogUs(toLogIn);
                System.out.println("Weiterleitung");
                return "success";
            } else {    
                logInMSG = "Passwort falsch. Bitte nochmal versuchen";
                return null;
            }
        }

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getLogInMSG() {
        return logInMSG;
    }

    public void setLogInMSG(String logInMSG) {
        this.logInMSG = logInMSG;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.db;

import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Employee;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Room;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class EmployeeDao {

    private static final String PERSISTENCE_UNIT_NAME = "kvt";
    private static EntityManagerFactory factory;
    private EntityManager em;

    public EmployeeDao() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        em = factory.createEntityManager();
    }

    public List<Employee> getAllEmployees() {
        Query q = em.createNamedQuery("Employee.findAll");
        List<Employee> userList = (List<Employee>) q.getResultList();

        return userList;
    }

    public Employee getEmployeeById(int userID) {
        Employee e = (Employee) em.find(Employee.class, userID);
        return e;
    }

    public List<Employee> getEmployeeByName(String firstname, String lastname) {
        Query q = em.createNamedQuery("Employee.findAll");
        List<Employee> empList = (List<Employee>) q.getResultList();
        List<Employee> resultList = new ArrayList<>();
        for(int i = 0; i < empList.size(); i++){
            if(empList.get(i).getFname().equals(firstname) && empList.get(i).getLname().equals(lastname)){
                resultList.add(empList.get(i));
            }
        }
        return resultList;
    }
    
   public List<Employee> getAllAdmins(){
       Query q = em.createNamedQuery("Employee.findAll");
       List<Employee> empList = (List<Employee>) q.getResultList();
       List<Employee> resultList = new ArrayList<>();
       
       for(int i = 0; i < empList.size(); i++){
           if(empList.get(i).getIsAdmin() == true){
               resultList.add(empList.get(i));
           }
       }
       return resultList;
   }
   
   public List<Employee> getEmployeesByDepartment(String dept){
       Query q = em.createNamedQuery("Employee.findAll");
       List<Employee> empList = (List<Employee>) q.getResultList();
       List<Employee> resultList = new ArrayList<>();
       
       for(int i = 0; i < empList.size(); i++){
           if(empList.get(i).getDept().equals(dept)){
               resultList.add(empList.get(i));
           }
       }
       return resultList;
   }
   
   public void addEmployee(Employee e){
       EntityTransaction entr = em.getTransaction();
       entr.begin();
       em.persist(e);
       entr.commit();
       em.close();
   }
   
   public void deleteEmployee(Employee e){
       EntityTransaction entr = em.getTransaction();
       entr.begin();
       Query query = em.createQuery("DELETE FROM Employee c WHERE c.employeeid = :r");
       query.setParameter("r", e.getUserID()).executeUpdate();
       entr.commit();
   
   }
   
    public void updateEmployee(Employee prev, Employee e){
        Employee emp = em.find(Employee.class, prev.getUserID());
        em.getTransaction().begin();
        emp.setFname(e.getFname());
        emp.setLname(e.getLname());
        emp.setMail(e.getMail());
        emp.setPassword(e.getPassword());
        emp.setDept(e.getDept());
        emp.setIsAdmin(e.getIsAdmin());
        em.getTransaction().commit();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Stefan Krojer
 */
@Entity
@Table(name = "employee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e")
    , @NamedQuery(name = "Employee.findByEmployeeid", query = "SELECT e FROM Employee e WHERE e.employeeid = :employeeid")
    , @NamedQuery(name = "Employee.findByFname", query = "SELECT e FROM Employee e WHERE e.fname = :fname")
    , @NamedQuery(name = "Employee.findByLname", query = "SELECT e FROM Employee e WHERE e.lname = :lname")
    , @NamedQuery(name = "Employee.findByMail", query = "SELECT e FROM Employee e WHERE e.mail = :mail")
    , @NamedQuery(name = "Employee.findByPassword", query = "SELECT e FROM Employee e WHERE e.password = :password")
    , @NamedQuery(name = "Employee.findByIsadmin", query = "SELECT e FROM Employee e WHERE e.isadmin = :isadmin")
    , @NamedQuery(name = "Employee.findByDept", query = "SELECT e FROM Employee e WHERE e.dept = :dept")})
public class Employee implements Serializable {

 private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "employeeid")
    private Integer employeeid;
    @Column(name = "fname")
    private String fname;
    @Column(name = "lname")
    private String lname;
    @Column(name = "mail")
    private String mail;
    @Column(name = "password")
    private String password;
    @Column(name = "isadmin")
    private Boolean isadmin;
    @Column(name = "dept")
    private String dept;
    @OneToMany(mappedBy = "employeeid")
    private Collection<Appointment> appointmentCollection;
    
    public Employee(String fName, String lName,String Password, String mail, String dept, boolean admin) {
        this.fname = fName;
        this.lname = lName;
        this.password = Password;
        this.mail = mail;
        this.dept = dept;
        this.isadmin = admin;
    }
    
    public Employee(int id, String fName, String lName,String Password, String mail, String dept, boolean admin) {
        this.employeeid = id;
        this.fname = fName;
        this.lname = lName;
        this.password = Password;
        this.mail = mail;
        this.dept = dept;
        this.isadmin = admin;
    }
       
    public Employee() {
        this.fname = "undefined";
        this.lname = "lName";
        this.password = "null";
        this.mail = "mail";
        this.dept = "dept";
        this.isadmin = false;
    }    

    public Integer getUserID() {
        return employeeid;
    }

    public void setUserID(Integer userID) {
        this.employeeid = userID;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getIsAdmin() {
        return isadmin;
    }

    public void setIsAdmin(Boolean isadmin) {
        this.isadmin = isadmin;
    }

    public Collection<Appointment> getAppointmentCollection() {
        return appointmentCollection;
    }

    public void setAppointmentCollection(Collection<Appointment> appointmentCollection) {
        this.appointmentCollection = appointmentCollection;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "User{" + "userID=" + employeeid + ", fname=" + fname + ", lname=" + lname + ", mail=" + mail + ", password=" + password + ", isAdmin=" + isadmin + ", appointmentCollection=" + appointmentCollection + ", dept=" + dept + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.employeeid);
        hash = 67 * hash + Objects.hashCode(this.fname);
        hash = 67 * hash + Objects.hashCode(this.lname);
        hash = 67 * hash + Objects.hashCode(this.mail);
        hash = 67 * hash + Objects.hashCode(this.password);
        hash = 67 * hash + Objects.hashCode(this.isadmin);
        hash = 67 * hash + Objects.hashCode(this.appointmentCollection);
        hash = 67 * hash + Objects.hashCode(this.dept);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (!Objects.equals(this.fname, other.fname)) {
            return false;
        }
        if (!Objects.equals(this.lname, other.lname)) {
            return false;
        }
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.dept, other.dept)) {
            return false;
        }
        if (!Objects.equals(this.employeeid, other.employeeid)) {
            return false;
        }
        if (!Objects.equals(this.isadmin, other.isadmin)) {
            return false;
        }
        if (!Objects.equals(this.appointmentCollection, other.appointmentCollection)) {
            return false;
        }
        return true;
    }
    
   
    
    
    
    
    

    
    
    
    
    
    
    
    
}

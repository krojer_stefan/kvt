/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.bean;

import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Appointment;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Approom;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Employee;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Room;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.service.Service;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import static java.time.ZoneId.SHORT_IDS;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

@ManagedBean
@SessionScoped
public class MainBean {

     @ManagedProperty(value = "#{service}")
    private Service service;
    private Employee currLoggedIn;
    private Date date;
    private double startAt;
    private double duration;
    private Appointment termin;
    private Room room;
     
    public MainBean() {
        System.out.println("MainBean created");
        currLoggedIn = service.getLogUs();
    }

        public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
    
    
    public void click() {
        System.out.println("Clicked");
        for(int i = 0; i<15; i++){
        service.sendMail(service.getUser("Lukas Hatzl").getUserID(), "Hallo!", "Hallo UwU");
        }
    }

    public void readCal() throws IOException {
        service.mapTermine(service.getLogUs(), service.readCSV("D:\\Schule\\Diplomarbeit\\KVT\\kvt\\Kalenderverwaltungstool\\cal.csv"));
    }

    public void printTermine() {
        System.out.println("Termine wie folgt: " + service.getAllTermine().size());
        for (Appointment t : service.getAllTermine()) {
            System.out.println(t);
        }
    }
    
    public void add()       
    {
        termin.setCreatedInKVT(true);
        termin.setDuration(duration);
        termin.setDate(date);
        termin.setRoom(room);
    }

    public Appointment getTermin() {
        return termin;
    }

    public void setTermin(Appointment termin) {
        this.termin = termin;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Employee getCurrLoggedIn() {
        return currLoggedIn;
    }

    public void setCurrLoggedIn(Employee currLoggedIn) {
        this.currLoggedIn = currLoggedIn;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getStartAt() {
        return startAt;
    }

    public void setStartAt(double startAt) {
        this.startAt = startAt;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
    
}

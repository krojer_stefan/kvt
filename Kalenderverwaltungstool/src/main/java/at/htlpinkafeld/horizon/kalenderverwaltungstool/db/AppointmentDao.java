/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.db;

import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Appointment;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Approom;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.ApproomPK;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Employee;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Room;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class AppointmentDao {

    private static final String PERSISTENCE_UNIT_NAME = "kvt";
    private static EntityManagerFactory factory;
    private EntityManager em;

    public AppointmentDao() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        em = factory.createEntityManager();
    }

    public List<Appointment> getAllAppointments() {
        Query q = em.createNamedQuery("Appointment.findAll");
        List<Appointment> appList = (List<Appointment>) q.getResultList();

        return appList;
    }

    public Appointment getByAppointmentId(int id) {
        Appointment a = (Appointment) em.find(Appointment.class, id);
        return a;
    }

    public List<Appointment> getByDate(Date app) {
        Query q = em.createNamedQuery("Appointment.findAll");
        List<Appointment> appList = (List<Appointment>) q.getResultList();
        List<Appointment> resList = new ArrayList<>();

        for (Appointment a : appList) {
            if (a.getDate().equals(app)) {
                resList.add(a);
            }
        }
        return resList;
    }

    public List<Appointment> getByDuration(double app) {
        Query q = em.createNamedQuery("Appointment.findByDuration");
        q.setParameter("duration", app);
        List<Appointment> appList = (List<Appointment>) q.getResultList();
        List<Appointment> resList = new ArrayList<>();
        double d = 0;
        for (Appointment a : appList) {
            d = a.getDuration().doubleValue();
            if (d == app){
                resList.add(a);
            }

        }
        return resList;
    }

    public List<Appointment> getByStartsAt(Appointment as) {
        Query q = em.createNamedQuery("Appointment.findByStartsat");
        q.setParameter("startsat", as.getStarsAt());
        List<Appointment> appList = (List<Appointment>) q.getResultList();
        List<Appointment> resList = new ArrayList<>();
        double d = 0;

        for (Appointment a : appList) {
            d = a.getStarsAt().doubleValue();
            if (d == as.getStarsAt()) {
                resList.add(a);
            }
        }
        return resList;
    }

    public List<Appointment> getStartsAtTime(double as) {
       Query q = em.createNamedQuery("Appointment.findByStartsat");
        q.setParameter("startsat", as);
        List<Appointment> appList = (List<Appointment>) q.getResultList();
        List<Appointment> resList = new ArrayList<>();
        double d = 0;

        for (Appointment a : appList) {
            d = a.getStarsAt().doubleValue();
            if (d == as) {
                resList.add(a);
            }
        }
        return resList;
    }

    public List<Appointment> getByKVT() {
        Query q = em.createNamedQuery("Appointment.findAll");
        List<Appointment> appList = (List<Appointment>) q.getResultList();
        List<Appointment> resList = new ArrayList<>();

        for (Appointment a : appList) {
            if (a.getCreatedInKVT() == true) {
                resList.add(a);
            }
        }
        return resList;
    }

    public List<Appointment> getByAllDay() {
        Query q = em.createNamedQuery("Appointment.findAll");
        List<Appointment> appList = (List<Appointment>) q.getResultList();
        List<Appointment> resList = new ArrayList<>();

        for (Appointment a : appList) {
            if (a.getAllday() == true) {
                resList.add(a);
            }
        }
        return resList;
    }

    public void addAppointment(Appointment app) {
            AppRoomDao ad = new AppRoomDao();
            Approom a = new Approom(app.getAppID(), app.getRoom().getRoomID());
            
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(app);
            entr.commit();
            em.close();
            a.setOccupied(true);
            ad.addApproom(a);
        
    }

    public void deleteAppointment(Appointment app) {
        AppRoomDao ad = new AppRoomDao(); 
        ad.deleteApproom(app);
        
        EntityTransaction entr = em.getTransaction();
        entr.begin();
        Query query = em.createQuery("DELETE FROM Appointment c WHERE c.appid = :r");
        query.setParameter("r", app.getAppID()).executeUpdate();
        entr.commit();
        em.close();
    }
    
    public void updateAppointment(Appointment prev, Appointment e){
        AppRoomDao ad = new AppRoomDao();
        ApproomPK apk = new ApproomPK(prev.getAppID(), prev.getRoom().getRoomID());
        Approom ar = em.find(Approom.class, apk);
        Appointment app = em.find(Appointment.class, prev.getAppID());
        
        em.getTransaction().begin();
        app.setAllday(e.getAllday());
        app.setBelongsTo(e.getBelongsTo());
        app.setCreatedInKVT(e.getCreatedInKVT());
        app.setDate(e.getDate());
        app.setDuration(e.getDuration());
        app.setRoom(e.getRoom());
        app.setStarsAt(e.getStarsAt());
        ad.updateApproom(prev, e);
        em.getTransaction().commit();
    }

}

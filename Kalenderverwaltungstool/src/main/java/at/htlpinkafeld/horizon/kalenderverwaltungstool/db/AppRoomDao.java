/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.horizon.kalenderverwaltungstool.db;

import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Appointment;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Approom;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.ApproomPK;
import at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Room;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;


public class AppRoomDao {
 private static final String PERSISTENCE_UNIT_NAME = "kvt";
    private static EntityManagerFactory factory;
    private EntityManager em;
    
    public AppRoomDao() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        em = factory.createEntityManager();
    }

    public void addApproom(Approom ar){
        ar.setOccupied(true);
        EntityTransaction entr = em.getTransaction();
        entr.begin();
        em.persist(ar);
        entr.commit();
        em.close();
    }
    
    public void deleteApproom(Appointment app){
        EntityTransaction entr = em.getTransaction();
        entr.begin();
        Query query = em.createQuery("DELETE FROM Approom c WHERE c.appointment.appid = :r");
        query.setParameter("r", app.getAppID()).executeUpdate();
        entr.commit();
        em.close();
    }
    
    public void updateApproom(Appointment a, Appointment b){
        ApproomPK apk = new ApproomPK();
        apk.setAppid(a.getAppID());
        apk.setRoomid(a.getRoom().getRoomID());
        Approom ar = em.find(Approom.class, apk);
        
        RoomDao rd = new RoomDao();
        Room r = rd.getRoomByID(b.getRoom().getRoomID());
        
        ApproomPK folAPK = new ApproomPK();
        folAPK.setAppid(b.getAppID());
        folAPK.setRoomid(r.getRoomID());
        
        em.getTransaction().begin();
        ar.setApproomPK(folAPK);
        ar.setOccupied(true);
        em.getTransaction().commit();
        em.close();
    }

    public boolean isAppointmentPossible (Appointment newApp) {
        Query q = em.createNamedQuery("Approom.findAll");
        List<Approom> apprList = (List<Approom>) q.getResultList();

        for (Approom a : apprList) {
            if (a.getAppointment().getDate() == newApp.getDate() && a.getAppointment().getAppID().equals(newApp.getAppID()) && a.getRoom().getRoomID().equals(newApp.getRoom())) {
                if (a.getAppointment().getAllday() == newApp.getAllday() || a.getAppointment().getStarsAt() == newApp.getStarsAt()) {
                    return false;
                } else if ((a.getAppointment().getDuration() + a.getAppointment().getStarsAt()) == (newApp.getStarsAt() + newApp.getDuration())) {
                    return false;
                }
            }

        }
        return true;
    }
}

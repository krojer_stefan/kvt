/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.horizon.kalenderverwaltungstool.bean;

import at.htlpinkafeld.horizon.kalenderverwaltungstool.service.Service;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.mail.MessagingException;
import javax.mail.Part;

/**
 *
 * @author Lukas
 */
@SessionScoped
@ManagedBean
public class SyncBean {

    @ManagedProperty(value =  "#{service}")
    private Service service;
    
    private Part file;
    
    public SyncBean() {
    }
    

public void save() throws IOException, MessagingException {
    try (InputStream input = file.getInputStream()) {
        Files.copy(input, new File("uploadpath", "filename").toPath());
    }
    catch (IOException e) {
    }
}

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;


@Embeddable
public class ApproomPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "appid")
    private int appid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "roomid")
    private int roomid;

    public ApproomPK() {
    }

    public ApproomPK(int appid, int roomid) {
        this.appid = appid;
        this.roomid = roomid;
    }

    public int getAppid() {
        return appid;
    }

    public void setAppid(int appid) {
        this.appid = appid;
    }

    public int getRoomid() {
        return roomid;
    }

    public void setRoomid(int roomid) {
        this.roomid = roomid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) appid;
        hash += (int) roomid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApproomPK)) {
            return false;
        }
        ApproomPK other = (ApproomPK) object;
        if (this.appid != other.appid) {
            return false;
        }
        if (this.roomid != other.roomid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.ApproomPK[ appid=" + appid + ", roomid=" + roomid + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "approom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Approom.findAll", query = "SELECT a FROM Approom a")
    , @NamedQuery(name = "Approom.findByAppid", query = "SELECT a FROM Approom a WHERE a.approomPK.appid = :appid")
    , @NamedQuery(name = "Approom.findByRoomid", query = "SELECT a FROM Approom a WHERE a.approomPK.roomid = :roomid")
    , @NamedQuery(name = "Approom.findByOccupied", query = "SELECT a FROM Approom a WHERE a.occupied = :occupied")})
public class Approom implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ApproomPK approomPK;
    @Column(name = "occupied")
    private Boolean occupied;
    @JoinColumn(name = "appid", referencedColumnName = "appid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Appointment appointment;
    @JoinColumn(name = "appid", referencedColumnName = "roomid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Room room;

    public Approom() {
    }

    public Approom(ApproomPK approomPK) {
        this.approomPK = approomPK;
    }

    public Approom(int appid, int roomid) {
        this.approomPK = new ApproomPK(appid, roomid);
    }

    public ApproomPK getApproomPK() {
        return approomPK;
    }

    public void setApproomPK(ApproomPK approomPK) {
        this.approomPK = approomPK;
    }

    public Boolean getOccupied() {
        return occupied;
    }

    public void setOccupied(Boolean occupied) {
        this.occupied = occupied;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (approomPK != null ? approomPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Approom)) {
            return false;
        }
        Approom other = (Approom) object;
        if ((this.approomPK == null && other.approomPK != null) || (this.approomPK != null && !this.approomPK.equals(other.approomPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "at.htlpinkafeld.horizon.kalenderverwaltungstool.POJO.Approom[ approomPK=" + approomPK + " ]";
    }

}
